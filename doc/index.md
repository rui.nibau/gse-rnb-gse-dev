Title:      GSE : rnb-gse-dev
Date:       2022-04-10
Cats:       informatique
Tags:       gnome-shell, extensions, nodejs, gnome, javascript
Technos:    gnome, javascript, nodejs
Intro:      Package nodejs qui facilite le développement d'extensions gnome-shell
Source:     https://framagit.org/rui.nibau/gse-rnb-gse-dev
Issues:     https://framagit.org/rui.nibau/gse-rnb-gse-dev/-/issues

## Présentation

°°todo°°A   écrire...

• It installs [ESLint](URL) and [Eslint-plugin-jsdoc](URL) globally.
• It makes symbolic links (``npm link``)

## Utilisation

### Import and install project

°°stx-bash°°
    git clone https://framagit.org/rui.nibau/gse-rnb-gse-dev.git
    cd gse-rnb-gse-dev
    npm install

### Use project in a already created extension

°°stx-bash°°
    cd /path/to/my-extension
    rnb-gse-dev init

### Use project to create a project extension

°°stx-bash°°
    rnb-gse-dev create /path/to/my-extension

## Ressources et references

• [Développement javascript sous Gnome](/articles/gjs)

## Historique

..include:: ./changelog.md

## Licence

..include:: ./licence.md

