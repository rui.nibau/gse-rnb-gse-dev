#!/bin/bash

create() {
    if [[ $1 ]]; then
        mkdir -p $1 && cd $1 && init
    fi
}

init() {
    if [[ -f jsconfig.json ]]; then
        rm jsconfig.json
    fi
    ln -sr ./node_modules/rnb-gse-dev/src/tpl.jsconfig.json jsconfig.json

    if [[ -f .eslintrc.yml ]]; then
        rm .eslintrc.yml
    fi
    ln -sr ./node_modules/rnb-gse-dev/src/tpl.eslintrc.yml .eslintrc.yml 
    
    npm link rnb-gse-dev 
    npm link eslint 
    npm link eslint-plugin-jsdoc
}

case $1 in
    create ) create $2;;
    init   ) init;;
esac
