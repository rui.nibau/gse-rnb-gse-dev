import * as Atk from '../node_modules/@gi-types/atk/index';
import * as Clutter from '../node_modules/@gi-types/clutter/index';
import * as Gio from '../node_modules/@gi-types/gio/index';
import * as GLib from '../node_modules/@gi-types/glib/index';
import * as GObject from '../node_modules/@gi-types/gobject/index';
import * as Graphene from '../node_modules/@gi-types/graphene/index';
import * as Meta from '../node_modules/@gi-types/meta/index';
import * as Shell from "../node_modules/@gi-types/shell/index";
import * as St from '../node_modules/@gi-types/st/index';

import * as Gtk from './node_modules/@gi-types/gtk/index';

// XXX Work in progress

/**
 * @typedef ExtensionMeta
 * @type {object}
 * @property {object} metadata - the metadata.json file, parsed as JSON
 * @property {string} uuid - the extension UUID
 * @property {number} type - the extension type; `1` for system, `2` for user
 * @property {Gio.File} dir - the extension directory
 * @property {string} path - the extension directory path
 * @property {string} error - an error message or an empty string if no error
 * @property {boolean} hasPrefs - whether the extension has a preferences dialog
 * @property {boolean} hasUpdate - whether the extension has a pending update
 * @property {boolean} canChange - whether the extension can be enabled/disabled
 * @property {string[]} sessionModes - a list of supported session modes
 */

class PanelMenuButtonBase extends St.Widget {
    setMenu(menu)
    menu
}

interface UiMainPanel extends St.Widget {
    addToStatusArea(role:string, indicator:Object, position?:Number, box?:('left'|'center'|'right'))
    statusArea: object.<string, PanelMenuButtonBase>
};

declare global {

    type ExtensionMeta = {
        /** the metadata.json file, parsed as JSON */
        metadata: {
            name: string
        } 
        /**the extension UUID*/
        uuid: string 
        // * @property {number} type - the extension type; `1` for system, `2` for user
        // * @property {Gio.File} dir - the extension directory
        // * @property {string} path - the extension directory path
        // * @property {string} error - an error message or an empty string if no error
        // * @property {boolean} hasPrefs - whether the extension has a preferences dialog
        // * @property {boolean} hasUpdate - whether the extension has a pending update
        // * @property {boolean} canChange - whether the extension can be enabled/disabled
        // * @property {string[]} sessionModes - a list of supported session modes
    };

    namespace imports {
        const gi = { Atk, Clutter, Gio, GLib, GObject, Graphene, Meta, Shell, St }
        namespace misc {
            namespace extensionUtils {
                const ExtensionType = {
                    SYSTEM: 1,
                    PER_USER: 2,
                };
                const ExtensionState = {
                    ENABLED: 1,
                    DISABLED: 2,
                    ERROR: 3,
                    OUT_OF_DATE: 4,
                    DOWNLOADING: 5,
                    INITIALIZED: 6,
                    UNINSTALLED: 99,
                };
                /**
                 * @returns {?object} - The current extension, or null if not called from an extension.
                 */
                function getCurrentExtension() : ?{
                    imports:Object,
                    metadata: {[key:string] : string},
                    uuid: String,
                    type: String,
                    dir: String,
                    path: String,
                    error: String,
                    hasPrefs: Boolean,
                    hasUpdate: Boolean,
                    canChange: Boolean,
                };
                /**
                 * initTranslations:
                 * @param {string=} domain - the gettext domain to use
                 *
                 * Initialize Gettext to load translations from extensionsdir/locale.
                 * If @domain is not provided, it will be taken from metadata['gettext-domain']
                 */
                function initTranslations(domain): void;
                /**
                 * getSettings:
                 * @param {string=} schema - the GSettings schema id
                 * @returns {Gio.Settings} - a new settings object for @schema
                 *
                 * Builds and returns a GSettings schema for @schema, using schema files
                 * in extensionsdir/schemas. If @schema is omitted, it is taken from
                 * metadata['settings-schema'].
                 */
                function getSettings(schema): Gio.Settings;
                /**
                 * openPrefs:
                 *
                 * Open the preference dialog of the current extension
                 */
                function openPrefs();
            }
        }

        interface AppFavorites extends St.Widget {
            getFavorites(): Shell.App[]
            getFavoriteMap(): Object.<string,Shell.App>
            isFavorite(appId:string): boolean
        }

        namespace ui {
            module appDisplay {
                class AppDisplay {

                }
                class AppIcon extends St.Widget {
                    constructor(app, options = {})
                    icon: object
                }
            }
            module appFavorites {
                function getAppFavorites(): AppFavorites
            }
            module dash {
                class DashIcon extends imports.ui.appDisplay.AppIcon {
                    // TODO
                }
                class DashItemContainer extends St.Widget {
                    // TODO
                }
                class ShowAppsIcon extends DashItemContainer {
                    // TODO
                }
                class DashIconsLayout extends Clutter.BoxLayout {}
                class Dash extends St.Widget {
                    showAppsButton:St.Button
                }
            }
            module main {
                const layoutManager: {
                    
                }
                const overview:overview.Overview

                const panel: UiMainPanel
                /**
                 * notify:
                 * @param {string} msg: A message
                 * @param {string} details: Additional information
                 */
                function notify(msg, details)
                /**
                 * notifyError:
                 * @param {string} msg: An error message
                 * @param {string} details: Additional information
                 *
                 * See shell_global_notify_problem().
                 */
                function notifyError(msg, details)
                
                const sessionMode: imports.ui.sessionMode

                /**
                 * activateWindow:
                 * @param {Object} window: the window to activate
                 * @param {Number} [time] current event time
                 * @param {Number} [workspaceNum]  window's workspace number
                 *
                 * Activates @window, switching to its workspace first if necessary,
                 * and switching out of the overview if it's currently active
                 */
                function activateWindow(window, time, workspaceNum)
            }
            module overview {
                class OverviewActor extends St.BoxLayout {
                    get dash()
                    get searchEntry()
                    get controls()
                    animateToOverview(state, callback:Function)
                    animateFromOverview(callback:Function)
                    runStartupAnimation(callback:Function)
                }
                class Overview {
                    get dash()
                    get dashIconSize():Number
                    get animationInProgress()
                    get visible():Boolean
                    get visibleTarget()
                    setMessage(text, options)
                    show(state = OverviewControls.ControlsState.WINDOW_PICKER)
                    hide()
                    toggle()
                    showApps()
                    getShowAppsButton():St.Button
                }
            }
            module panelMenu {
                class Button extends PanelMenuButtonBase {};
            }
        
            module popupMenu {
                class PopupBaseMenuItem extends St.BoxLayout {
                    activate(event:Object)
                    active:Boolean
                    sensitive:Boolean
                }
                class PopupMenuItem extends PopupBaseMenuItem {
                    label:St.Label
                    label_actor:St.Label
                }
                class PopupSeparatorMenuItem extends PopupBaseMenuItem {
                    label:St.Label
                    label_actor:St.Label
                }
                class Switch extends St.Bin {
                    state:Boolean 
                }
                class PopupSwitchMenuItem extends PopupBaseMenuItem {
                    label:St.Label
                    toggle()
                    get state():Boolean
                    setToggleState(state:Boolean)
                    setStatus(text:string)
                }
                class PopupImageMenuItem extends PopupBaseMenuItem {
                    setIcon(icon:Gio.Icon)
                }
                class PopupMenuBase {
                    length:Number
                    sensitive:Boolean
                    addAction(title:string, callback:Function, icon:Gio.Icon)
                    addSettingsAction(title, desktopFile)
                    isEmpty():Boolean
                    itemActivated(animate)
                    moveMenuItem(menuItem, position)
                    addMenuItem(menuItem, position)
                    get firstMenuItem():PopupBaseMenuItem
                    get numMenuItems():Boolean
                    removeAll()
                    toggle()
                    destroy()
                }
                class PopupMenu extends PopupMenuBase {
                    actor
                    open(animate)
                    close(animate)
                    isOpen:Boolean
                }
            }

            module sessionMode {
                class SessionMode {

                }
            }
        }
    }
    
    const global;

    function print(...args: any[]): void;
    function printerr(...args: any[]): void
    function log(message?: string): void
    function logError(exception: any, message?: string): void
}
