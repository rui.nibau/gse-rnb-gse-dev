import * as Atk from '../node_modules/@gi-types/atk/index';
import * as Clutter from '../node_modules/@gi-types/clutter/index';
import * as Gio from '../node_modules/@gi-types/gio/index';
import * as GLib from '../node_modules/@gi-types/glib/index';
import * as GObject from '../node_modules/@gi-types/gobject/index';
import * as Graphene from '../node_modules/@gi-types/graphene/index';
import * as Meta from '../node_modules/@gi-types/meta/index';
import * as Shell from "../node_modules/@gi-types/shell/index";
import * as St from '../node_modules/@gi-types/st/index';

export { Atk, Clutter, Gio, GLib, GObject, Graphene, Meta, Shell, St };
