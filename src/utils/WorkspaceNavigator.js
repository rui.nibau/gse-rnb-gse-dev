const { Clutter } = imports.gi;

const Main = imports.ui.main;

const ws_manager = global.workspace_manager;

/**
 * Simple workspace navigator
 * 
 * @param {import('gi').Clutter.Actor} actor 
 */
var WorkspaceNavigator = class WorkspaceNavigator {
    /**
     * @param {import('gi').Clutter.Actor} actor 
     */
    constructor(actor = null) {
        
        /**@type {import('gi').Clutter.Actor} */
        this._actor = actor;

        if (this._actor) {
            /**@type {number}*/
            this._scrollEventId = this._actor.connect_after('scroll-event', 
                /**
                 * @param {import('gi').Clutter.Actor} actor 
                 * @param {import('gi').Clutter.Event} event 
                 */
                (actor, event) => {
                    const source = event.get_source();
                    // scroll on right box : just propagate event
                    if (source !== actor && Main.panel._rightBox && Main.panel._rightBox.contains 
                            && Main.panel._rightBox.contains(source)) {
                        return Clutter.EVENT_PROPAGATE;
                    }
                    // Scroll to workspace
                    let changed = false;
                    switch (event.get_scroll_direction()) {
                        case Clutter.ScrollDirection.DOWN:
                        case Clutter.ScrollDirection.RIGHT:
                            changed = this.next();
                            break;
                        case Clutter.ScrollDirection.UP:
                        case Clutter.ScrollDirection.LEFT:
                            changed = this.previous();
                            break;
                    }
                    return changed ? Clutter.EVENT_STOP : Clutter.EVENT_PROPAGATE;
            });
        }
    }

    destroy() {
        if (this._actor) {
            this._actor.disconnect(this._scrollEventId);
            this._scrollEventId = null;
            this._actor = null;
        }
    }

    /**
     * Activate a workspace
     * 
     * @param {Number} index of the workspace
     * @return {Boolean} If a new workspace has been activated
     */
     activate(index) {
        if (index >= 0 && index < ws_manager.n_workspaces) {
            const active_ws = ws_manager.get_active_workspace();
            const new_ws = ws_manager.get_workspace_by_index(index);
            if (new_ws !== active_ws) {
                new_ws.activate(global.get_current_time());
                return true;
            }
        }
        return false;
    }

    /**
     * Activate the next workspace if any
     * 
     * @returns {Boolean} If active workspace has been changed
     */
    next() {
        /**@type {import('gi').Meta.Workspace} */
        const active_ws = ws_manager.get_active_workspace();
        const new_ws = active_ws.get_neighbor(Meta.MotionDirection.RIGHT);
        if (new_ws) {
            new_ws.activate(global.get_current_time());
            return true;
        }
        return false;
    }

    /**
     * Activate the previous workspace if any
     * 
     * @returns {Boolean} If active workspace has been changed
     */
    previous() {
        /**@type {import('gi').Meta.Workspace} */
        const active_ws = ws_manager.get_active_workspace();
        const new_ws = active_ws.get_neighbor(Meta.MotionDirection.LEFT);
        if (new_ws) {
            new_ws.activate(global.get_current_time());
            return true;
        }
        return false;
    }
}
