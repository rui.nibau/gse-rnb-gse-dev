const { Meta } = imports.gi;
const Main = imports.ui.main;

const DELTA = 10;

/**
 * Track window position relative to a specified actor
 */
var WindowTacker = class WindowTracker {

    /**
     * @param {import('gi').Clutter.Actor} actor 
     */
    constructor(actor) {

        /**@type {import('gi').Clutter.Actor} */
        this._actor = actor;

        this._overlap = false;

        /**@type {Map.<object, number[]>}*/
        this._connections = new Map();
        for (const metaWindowActor of global.get_window_actors()) {
            this._on_win_actor_added(metaWindowActor.get_parent(), metaWindowActor);
        }
        this._connections.set(global.window_group, [
            global.window_group.connect('actor-added', this._on_win_actor_added.bind(this)),
            global.window_group.connect('actor-removed', this._on_win_actor_removed.bind(this))
        ]);
        this._connections.set(global.window_manager, [
            global.window_manager.connect('switch-workspace', this.update.bind(this))
        ]);
    }

    destroy() {
        this._connections.forEach((ids, object) => ids.forEach(id => object.disconnect(id)));
        this._connections.clear();
        this._actor = null;
    }

    update() {
        if (Main.panel.has_style_pseudo_class('overview') || !Main.sessionMode.hasWindows
                || !Main.layoutManager.primaryMonitor) {
            return;
        }

        // Windows in primary monitor
        const ws = global.workspace_manager.get_active_workspace();
        /**@type {import('gi').Meta.Window[]}*/
        const windows = ws.list_windows().filter((/**@type {import('gi').Meta.Window}*/win) => {
            return win.is_on_primary_monitor()
                    && win.showing_on_its_workspace()
                    && !win.is_hidden()
                    && win.get_window_type() !== Meta.WindowType.DESKTOP;
        });

        // if window overlaps actor.
        /**@type {import('gi').Graphene.Rect} */
        const actor_extends = this._actor.get_transformed_extents();
        const actor_rect = new Meta.Rectangle({
            x: actor_extends.get_x() + DELTA,
            y: actor_extends.get_y() + DELTA,
            width: actor_extends.get_width() + DELTA,
            height: actor_extends.get_height() + DELTA
        });
        const overlap = windows.some(metaWindow => {
            const win_rect = metaWindow.get_frame_rect();
            return win_rect.overlap(actor_rect);
        });

        this.overlap = overlap;
    }

    get overlap () {
        return this._overlap;
    }
    
    /**
     * 
     * @param {boolean} overlap 
     */
    set overlap(overlap) {
        if (overlap !== this._overlap) {
            log(`WindowTracker: overlap: ${overlap}`);
            this._overlap = overlap;
            // FIXME emit event
        }
    }

    /**
     * @param {Object} container 
     * @param {Object} metaWindowActor 
     */
     _on_win_actor_added(container, metaWindowActor) {
        this._connections.set(metaWindowActor, [
            metaWindowActor.connect('notify::allocation', this.update.bind(this)),
            metaWindowActor.connect('notify::visible', this.update.bind(this))
        ]);
    }

    /**
     * @param {Object} container 
     * @param {Object} metaWindowActor 
     */
    _on_win_actor_removed(container, metaWindowActor) {
        const ids = this._connections.get(metaWindowActor);
        if (ids) {
            ids.forEach(id => metaWindowActor.disconnect(id));
        }
        this._connections.delete(metaWindowActor);
        this.update();
    }

}

// https://gitlab.gnome.org/GNOME/gjs/-/blob/master/modules/core/_signals.js