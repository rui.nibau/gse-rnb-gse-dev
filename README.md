# rnb-gse-dev

Simple node package to ease gnome-shell extension development

* [Website](https://omacronides.com/projets/gse-rnb-gse-dev)
* [sources](https://framagit.org/rui.nibau/gse-rnb-gse-dev)
* [Issues](https://framagit.org/rui.nibau/gse-rnb-gse-dev/-/issues)

